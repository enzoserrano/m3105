#!/usr/bin/env bash

## configuration de wiki.org 
# configuration du serveur dns

# La commande ci-dessous crée un répertoire /etc/named sur la machine dwikiorg
himage dwikiorg mkdir -p /etc/named

# La commande ci-dessous copie le fichier se trouvant dans wiki.org/named.conf sur la machine dwikiorg dans le répertoire /etc/
hcp wiki.org/named.conf dwikiorg:/etc/.

# La commande ci-dessous copie tous les fichiers se trouvant dans wiki.org/* dans le répertoire  /etc/named sur la machine dwikiorg
hcp wiki.org/* dwikiorg:/etc/named/.

# la commande ci-dessou supprime (commande rm) le fichier /etc/named/named.conf se trouvant sur la machine dwikiorg
himage dwikiorg rm /etc/named/named.conf 


## configuration de iut.re 
# configuration du serveur dns
# Les fichiers ci-dessous ne sont pas présents
# du coup je commentes

#himage diutre mkdir -p /etc/named
#hcp iut.re/named.conf diutre:/etc/.
#hcp iut.re/* diutre:/etc/named/.
#himage diutre rm /etc/named/named.conf 


## configuration de pc1
# resolv.conf
# Les fichiers ci-dessous ne sont pas présents
# du coup je commentes

#hcp pc1/resolv.conf pc1:/etc/.


#configuration de rtiutre
himage drtiutre mkdir -p /etc/named
hcp rt.iut.re/named.conf drtiutre:/etc/.
hcp rt.iut.re/* drtiutre:/etc/named/.
himage drtiutre rm /etc/named/named.conf 

#configuration de iutre
himage diutre mkdir -p /etc/named
hcp iut.re/named.conf diutre:/etc/.
hcp iut.re/* diutre:/etc/named/.
himage diutre rm /etc/named/named.conf

#configuration TLDdorg
himage dorg mkdir -p /etc/named
hcp tldorg/named.conf dorg:/etc/.
hcp tldorg/* dorg:/etc/named/.
himage dorg rm /etc/named/named.conf



#configuration TLDre
himage dre mkdir -p /etc/named
hcp tldre/named.conf dre:/etc/.
hcp tldre/* dre:/etc/named/.
himage dre rm /etc/named/named.conf


#aRootServer
himage aRootServer mkdir -p /etc/named
hcp aRootServer/named.conf aRootServer:/etc/.
hcp aRootServer/* aRootServer:/etc/named/.
himage aRootServer rm /etc/named/named.conf


